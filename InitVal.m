
classdef InitVal
    %STARTOPTIONS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Constant)
        height = 0.1;
        nH = 4;
        width = 0.1; % broadness
        nL = 4; % nb
        delta_tau = 50;
        tau = 500;
        k=25;
        alpha = 300;
        T0 = 100;
        T_ambient = 1200;
        c= 700;
        rho = 7800;
    end
    
    methods
    end
    
end

