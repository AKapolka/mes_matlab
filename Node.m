classdef Node
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        x % real
        y % real
        column % local
        row % local
        T % temp
    end
    
    methods
        function obj = Node(index)
            if nargin > 0
               index = 0;
            end
%             obj.column = ceil(index/InitVal.nH);
%             obj.row = index - InitVal.nH * (obj.column - 1) ;
            [obj.row, obj.column] = ind2sub([InitVal.nH, InitVal.nL],index);
            obj.x = (obj.column - 1) * InitVal.width / (InitVal.nL -1);
            obj.y = (obj.row - 1) * InitVal.height / (InitVal.nH - 1);
            obj.T = InitVal.T0;
        end
        function obj = set(obj, index)
%             obj.column = ceil(index/InitVal.nH);
%             obj.row = index - InitVal.nH * (obj.column - 1) ;
            [obj.row, obj.column] = ind2sub([InitVal.nH, InitVal.nL],index);
            obj.x = (obj.column - 1) * InitVal.width / (InitVal.nL -1);
            obj.y = (obj.row - 1) * InitVal.height / (InitVal.nH - 1);
            obj.T = InitVal.T0;
        end
        
    end
    
end

