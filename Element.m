classdef Element
    %UNTITLED4 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        column
        row
        ID = [-1:-1:-4];
        edges = zeros(1,4);
    end
    
    methods
        function obj = Element(index)
            if nargin > 0
               index = 0;
            end
            if index > (InitVal.nH -1) * (InitVal.nL - 1)
               error('overflow') 
            end
%            obj.column = ceil(index/(InitVal.nH -1));
%            obj.row = InitVal.nH - 2 - index + (InitVal.nH -1) * (obj.column - 1);
            [obj.row, obj.column] = ind2sub([InitVal.nH - 1, InitVal.nL - 1],index);
            obj.ID(1) = (obj.column - 1) * InitVal.nH + obj.row;
            obj.ID(2) = obj.ID(1) + InitVal.nH;
            obj.ID(3) = obj.ID(2) + 1;
            obj.ID(4) = obj.ID(1) + 1;

            if obj.row == 1
              obj.edges(1) = 1; % S
            end
            if obj.column == InitVal.nL -1
              obj.edges(2) = 1; % E
            end
            if obj.row == InitVal.nH -1
              obj.edges(3) = 1; % N
            end
            if obj.column == 1
              obj.edges(4) = 1; % W
            end
        end
        
        function obj = set(obj, index)
            if index > (InitVal.nH -1) * (InitVal.nL - 1)
               error('overflow') 
            end
%            obj.column = ceil(index/(InitVal.nH -1));
%            obj.row = InitVal.nH - 2 - index + (InitVal.nH -1) * (obj.column - 1);
            [obj.row, obj.column] = ind2sub([InitVal.nH - 1, InitVal.nL - 1],index);
            obj.ID(1) = (obj.column - 1) * InitVal.nH + obj.row;
            obj.ID(2) = obj.ID(1) + InitVal.nH;
            obj.ID(3) = obj.ID(2) + 1;
            obj.ID(4) = obj.ID(1) + 1;

            obj.edges = zeros(1,4);
            if obj.row == 1
              obj.edges(1) = 1; % S
            end
            if obj.column == InitVal.nL -1
              obj.edges(2) = 1; % E
            end
            if obj.row == InitVal.nH -1
              obj.edges(3) = 1; % N
            end
            if obj.column == 1
              obj.edges(4) = 1; % W
            end
        end
    end
    
end

