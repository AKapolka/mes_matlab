function [h,p,c] = local_matricies(grid, element)
    x = zeros(4);
    y = zeros(4);
    for i = 1:4
       node_idx = grid.elements(element).ID(i);
       x(i) = grid.nodes(node_idx).x;
       y(i) = grid.nodes(node_idx).y;
    end
    
    num_of_int_pts = 4;
    coord = 1/ sqrt(3);
    coord_list = [-coord,coord];
    ksi = [-coord,coord,coord,-coord];
    eta = [-coord,-coord,coord,coord];
    weights = [1,1];
    
    % shape functions
    shape_functions = zeros(4,num_of_int_pts);
    shape_functions(1,:) = 1/4 * (1-ksi) .* (1-eta);
    shape_functions(2,:) = 1/4 * (1+ksi) .* (1-eta);
    shape_functions(3,:) = 1/4 * (1+ksi) .* (1+eta);
    shape_functions(4,:) = 1/4 * (1-ksi) .* (1+eta);
    
    % shape functions derivatives
    dn_dksi = zeros(4,num_of_int_pts);
    dn_deta = zeros(4,num_of_int_pts);
    
    dn_dksi(1,:) = -1/4 * (1-eta);
    dn_dksi(2,:) = 1/4 * (1-eta);
    dn_dksi(3,:) = 1/4 * (1+eta);
    dn_dksi(4,:) = -1/4 * (1+eta);
    
    dn_deta(1,:) = -1/4 * (1-ksi);
    dn_deta(2,:) = -1/4 * (1+ksi);
    dn_deta(3,:) = 1/4 * (1+ksi);
    dn_deta(4,:) = 1/4 * (1-ksi);
    
    % Jacobian
    jacobian_matrix = zeros(4, num_of_int_pts);
    
    for i= 1:num_of_int_pts
        jacobian_matrix(1,:) = jacobian_matrix(1,:) + dn_dksi(i,:) * x(i);
        jacobian_matrix(2,:) = jacobian_matrix(2,:) + dn_dksi(i,:) * y(i);
        jacobian_matrix(3,:) = jacobian_matrix(3,:) + dn_deta(i,:) * x(i);
        jacobian_matrix(4,:) = jacobian_matrix(4,:) + dn_deta(i,:) * y(i);
    end
    jacobian_matrix;
    
    % jacobian determinant
    det_j = zeros(1,num_of_int_pts);
    
    for i= 1:num_of_int_pts
        det_j(i) = det(reshape(jacobian_matrix(:,i),[2,2]));
    end
    
    % inverted jacobian
    inv_matrix = zeros(4,num_of_int_pts);
    for i = 1:num_of_int_pts
        inv_matrix(:,i) = reshape(inv(reshape(jacobian_matrix(:,i),[2,2])),[4,1]);
    end

    % derivatives by x,y
    dn_dx = zeros(4,num_of_int_pts);
    dn_dy = zeros(4,num_of_int_pts);
    
    for i = 1:4
       dn_dx(i,:) = dn_dx(i,:) + inv_matrix(1,:) .* dn_dksi(i,:) + inv_matrix(2,:) .* dn_deta(i,:);
       dn_dy(i,:) = dn_dy(i,:) + inv_matrix(3,:) .* dn_dksi(i,:) + inv_matrix(4,:) .* dn_deta(i,:);
    end
    
    % Matrix H
    h = zeros(4,4);
    
    for i = 1:num_of_int_pts
       a = dn_dx(:,i)*dn_dx(:,i)' ;
       b = dn_dy(:,i)*dn_dy(:,i)' ;
       e = (a+b) * det_j(i) * weights(1+mod(i, 2)) * weights(1 + floor((i-1)/2));
       h = h+e;
    end
    
    h=h*InitVal.k;
    
    % Matrix C
    c = zeros(4,4);
    
    for i = 1:num_of_int_pts
        n_t_n = shape_functions(:,i)*shape_functions(:,i)';
        e = n_t_n * det_j(i) * weights(1+mod(i, 2)) * weights(1 + floor((i-1)/2));
        c = c + e;

    end
    
    c = c * InitVal.rho * InitVal.c;
    
    % Matrix H part #2, vector P
    
    length_h = InitVal.height / (InitVal.nH - 1);
    length_l = InitVal.width / (InitVal.nL - 1);
    surface_det_j = [length_l / 2, length_h / 2,length_l / 2, length_h / 2];
    surface_ksi = [coord_list];
    
    surface_shape = zeros(2,2);
    surface_shape(1,:) = 1/2*(1-surface_ksi);
    surface_shape(2,:) = 1/2*(1+surface_ksi);
    
    surface_h = zeros(4,4);
    p = zeros(4,1);
    for i = 1:4
        % Check for boundry condition
       if grid.elements(element).edges(i)
          result_p = zeros(2,1);
          result = zeros(2,2);
          for j = 1:2
             result = result + surface_shape(:,j) * surface_shape(:,j)' * surface_det_j(i) * weights(j);
             result_p = result_p + surface_shape(:,j) * surface_det_j(i) * weights(j);
          end
          result = result * InitVal.alpha;
          indexes = [i, max(mod(i+1,5),1)];
          p(indexes(1)) = p(indexes(1)) + result_p(1);
          p(indexes(2)) = p(indexes(2)) + result_p(2);
          for j = 1:2
             for k = 1:2
                surface_h(indexes(j),indexes(k)) = surface_h(indexes(j),indexes(k)) + result(j,k);
             end
          end
       end
    end
    p = p * InitVal.alpha * InitVal.T_ambient;
        
    h = h + surface_h;
    
    %return h,p,c
    
    
end




