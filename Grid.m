classdef Grid
    %UNTITLED5 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        nodes = Node(1)
        elements = Element(1)
    end
    
    methods
        function obj=Grid()
            num_nodes = InitVal.nH * InitVal.nL;
            num_elem = (InitVal.nH - 1) * (InitVal.nL - 1);
            obj.nodes(num_nodes:-1:1) = Node(1);
            obj.elements(num_elem:-1:1) = Element(1);
            for n=1:num_nodes
               obj.nodes(n) = obj.nodes(n).set(n); 
            end
            for n=1:num_elem
               obj.elements(n) = obj.elements(n).set(n); 
            end
            
        end
        
%         function obj=set(obj)
%             num_nodes = InitVal.nH * InitVal.nL;
% 
%             for n=1:num_nodes
%                    disp(n)
%                    obj.nodes(n).set(n); 
%             end
%         end
    end
    
end

