clear;

a = Grid();

num_nodes = InitVal.nH * InitVal.nL;
num_elem = (InitVal.nH - 1) * (InitVal.nL - 1);

h_global = zeros(num_nodes,num_nodes);
c_global = zeros(num_nodes,num_nodes);
p_global = zeros(num_nodes,1);
T0 = ones(num_nodes,1) * InitVal.T0;
T1 = zeros(num_nodes,1);

% Aggregation
for i = 1:length(a.elements)
   [h,p,c] = local_matricies(a, i);
   for j = 1:4
       index1 = a.elements(i).ID(j);
       p_global(index1) = p_global(index1) + p(j);
       for k = 1:4
           index2 = a.elements(i).ID(k);
           h_global(index1,index2) = h_global(index1,index2) + h(j,k);
           c_global(index1,index2) = c_global(index1,index2) + c(j,k);
       end
   end
end

h_dash = h_global + c_global / InitVal.delta_tau;
h_dash_inv = inv(h_dash);

% Going through steps
for i = 1:floor(InitVal.tau / InitVal.delta_tau)
   fprintf('Iteracja: %d\nTime: %ds\n', i, i * InitVal.delta_tau)
   c_T0 = (c_global / InitVal.delta_tau) * T0;
   p_dash = c_T0 + p_global;
   T1 = h_dash_inv * p_dash;
   T0 = T1;
   fprintf('Min: %f\tMax: %f\n\n',min(min(T1)),max(max(T1)))
end

